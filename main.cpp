#include "iostream"
#include "cmath"
#include "vector"
#include "string"

#define PRIM_NUMBERS 1000000
#define isPrime(x) (!primes[x])

using namespace std;

bool primes[PRIM_NUMBERS] = {false};		// sieve of eratosthenes

void init_primes()							// init sieve
{
	primes[1] = true;
	
	for(int i = 2; i < PRIM_NUMBERS; i++)
	{
		if(primes[i])
			continue;
		
		for(int j = i * 2; j < PRIM_NUMBERS; j += i)
			primes[j] = true;
	}
}

int gcd(int a, int b)						// greatest common divisor
{
	int w = 1;
	
	while(b > 0)
	{
		w = a % b;
		a = b;
		b = w;
	}
	
	return a;
}

int findPrime(int x)						// finding coprime
{
	for(int i = 2; i < x; i++)
	{
		if(gcd(x, i) == 1)
			return i;
	}
	
	return 0;
}

int findd(int e, int fi)
{
	int d = 1;
	
	while(d++)
	{
		if(!(((d * e - 1) / e) % fi))
			return d;
	}
	
	return d;
}

int modExp(int b, int e, int m)
{
	int c = 1;
	
	for(int i = 0; i < e; i++)
		c = (b * c) % m;
		
	return c;
}

string cipher(string m, int e, int n)
{
	string out = m;
	
	for(int i = 0; i < m.length(); i++)
		out[i] = modExp(m[i], e, n);
	
	return out;
}

int main()
{
	init_primes();
	
	int p, q;
	string m;
	
	cout << "Prezentacja algorytmu RSA.\n";
	cout << "Prosze wprowadzic dane do zaszyfrowania:\n";
	getline(cin, m);
	cout << "Prosze wprowadzic dwie liczby pierwsze:\n";
	cin >> p;
	cin >> q;
	
	long unsigned int n = p * q;			// calculating n
	int fi = (p - 1) * (q - 1);
	
	int e = findPrime(fi);
	if(!e)
	{
		cout << "Cos poszlo nie tak.\nMozliwe przyczyny:\n\tWprowadzono zbyt duze liczby pierwsze\n\tWprowadzono liczby nie bedace pierwszymi\n";
		return 0;
	}
	
	int d = findd(e, fi);
	
	string c = cipher(m, e, n);
	string dec = cipher(m, d, n);
	
	cout << "Oto zaszyfrowane dane:\n";
	cout << c << "\n";
	cout << "Oto odszyfrowane dane:\n";
	cout << dec << "\n";
	
	return 0;
}
